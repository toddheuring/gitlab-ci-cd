#!/usr/bin/env bash
# Build
docker build -t api -f Dockerfile-Api .
docker build -t api.functional -f Dockerfile-ApiFunctionalTest .
docker build -t flyway -f Dockerfile-Flyway .

# Run
docker run --rm -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=Password1' -d --name db.af microsoft/mssql-server-linux:latest
docker run --rm --link=db.af:db --name api.af -d api

# Migrate
sleep 10
docker exec db.af /opt/mssql-tools/bin/sqlcmd -S 'tcp:localhost,1433' -U sa -P Password1 -Q 'CREATE DATABASE gitlab_ci_cd'
docker run --rm --link=db.af:db flyway -url='jdbc:sqlserver://db;databaseName=gitlab_ci_cd' -user=sa -password=Password1 -locations=filesystem:/app/sql migrate
docker exec db.af /opt/mssql-tools/bin/sqlcmd -S 'tcp:localhost,1433' -U sa -P Password1 -d gitlab_ci_cd -Q 'SELECT * FROM flyway_schema_history'

# Test
docker run --link=api.af:apiurl --rm  api.functional

# Cleanup
docker stop api.af
docker stop db.af