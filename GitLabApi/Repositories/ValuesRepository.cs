using System.Collections.Generic;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace GitLabApi.Repositories
{
    public interface IValuesRepository
    {
        IEnumerable<string> Get();
    }

    public class ValuesRepository : Repository, IValuesRepository
    {
        public ValuesRepository(IConfiguration config) : base(config)
        {
        }

        public IEnumerable<string> Get()
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.Query<string>("SELECT Value FROM [Values]");
            }
        }
    }
}