﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GitLabApi.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace GitLabApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly IValuesRepository _valuesRepository;

        public ValuesController(IValuesRepository valuesRepository)
        {
            _valuesRepository = valuesRepository;
        }

        [HttpGet]
        public IEnumerable<string> Get()
        {
            return _valuesRepository.Get();
        }

        [HttpPost]
        public string Post([FromBody] string value)
        {
            return value;
        }
    }
}
