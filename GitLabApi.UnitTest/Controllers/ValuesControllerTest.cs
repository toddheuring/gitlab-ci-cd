using NUnit.Framework;
using System.Collections.Generic;
using GitLabApi.Controllers;
using Moq;
using GitLabApi.Repositories;

namespace GitLabApi.UnitTest
{
    public class ValuesControllerTest
    {
        Mock<IValuesRepository> _valuesRepository;
        ValuesController _controller;

        [SetUp]
        public void Setup()
        {
            _valuesRepository = new Mock<IValuesRepository>(MockBehavior.Strict);

            _controller = new ValuesController(_valuesRepository.Object);
        }

        [Test]
        public void Get()
        {
            IEnumerable<string> expectedValues = new List<string> { "value1", "value2" };

            _valuesRepository.Setup(x => x.Get())
                .Returns(expectedValues);


            var result = _controller.Get();


            Assert.That(result, Is.SameAs(expectedValues));
        }
    }
}