using System;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using Newtonsoft.Json;
using NUnit.Framework;

namespace GitLabApi.FunctionalTest
{
    public class ValuesTest
    {
        private HttpClient _client;

        [SetUp]
        public void Setup()
        {
            _client = new HttpClient { BaseAddress = new Uri("http://apiurl/api/") };
        }

        [Test]
        public void ValuesControllerReturnsValues()
        {
            var response = _client.GetAsync("values").Result;

            string[] expectedValues = { "value1", "value2" };
            var expectedJson = JsonConvert.SerializeObject(expectedValues);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.That(expectedJson, Is.EqualTo(response.Content.ReadAsStringAsync().Result));
        }

        [Test]
        public void ValuesControllerPost()
        {
            var stringContent = new StringContent(JsonConvert.SerializeObject("value"), Encoding.UTF8, "application/json");
            
            var response = _client.PostAsync("values", stringContent).Result;
            
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.That("value", Is.EqualTo(response.Content.ReadAsStringAsync().Result));
        }
    }
}