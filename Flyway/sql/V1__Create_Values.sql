CREATE TABLE [Values] (
    Id INT Identity PRIMARY KEY,
    Value VARCHAR(255) NOT NULL
)